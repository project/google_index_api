<?php

namespace Drupal\google_index_api\Service;

use Drupal\Core\State\StateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Google_Client;
use Drupal\file\Entity\File;

/**
 * Service class for calling the Google Index API.
 *
 * @ingroup google_index_api
 */
class GoogleIndexApi {

  /**
   * The Google Index API End Point.
   */
  const END_POINT = 'https://indexing.googleapis.com/v3/urlNotifications:publish';

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The Google Client.
   *
   * @var \Google_Client
   */
  protected $client;

  /**
   * Callback Controller constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The State Service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The Logging Service.
   */
  public function __construct(StateInterface $state, LoggerChannelFactoryInterface $loggerFactory) {
    $this->state = $state;
    $this->loggerFactory = $loggerFactory->get('Google Index API');
    $this->initalizeClient();
  }

  /**
   * Setup Google Client on instantiation.
   */
  protected function initalizeClient() {
    $file = $this->state->get('google_index_api_json_file');
    // Check our file and load the settings.
    if (isset($file[0])) {
      $this->client = new Google_Client();
      $uri = File::load($file[0])->getFileUri();
      $this->client->setAuthConfig($uri);
      $this->client->addScope('https://www.googleapis.com/auth/indexing');
    }
    else {
      $this->loggerFactory->error('Unable to get json file, please visit the <a href="%url">Setting Page</a> and upload a json file', [
        '%url' => '/admin/config/services/google-index-api'
      ]);
    }
  }

  /**
   * Update url in the Google Index API.
   *
   * @param string $url
   *   The url we are updating.
   */
  public function updateUrl($url) {
    $this->callApi($url, 'URL_UPDATED');
  }

  /**
   * Delete url from the Google Index API.
   *
   * @param string $url
   *   The url we are deleting.
   */
  public function deleteUrl($url) {
    $this->callApi($url, 'URL_DELETED');
  }

  /**
   * Mechanism to call the API.
   *
   * @param string $url
   *   The url we are calling.
   * @param string $type
   *   The action we are calling.
   */
  protected function callApi($url, $type) {
    $domain = $this->state->get('google_index_api_base_domain');
    $content = [
      'type' => $type,
      'url' => $domain . $url,
    ];

    $http_client = $this->client->authorize();
    /** @var \GuzzleHttp\Psr7\Response $response */
    $response = $http_client->post(self::END_POINT, ['body' => json_encode($content)]);
    $status_code = $response->getStatusCode();

    // Let the peoples know what happened.
    switch ($status_code) {
      case '200':
        $action = $type === 'URL_UPDATED' ? 'updated' : 'deleted';
        $this->loggerFactory->notice('Successfully %action %url', [
          '%action' => $action,
          '%url' => $url
        ]);
        break;

      default:
        $body = json_decode($response->getBody()->getContents(), TRUE);
        $msg = $body["error"]["message"] ?? 'Google Index API Returned a Status code of ' . $status_code;
        $this->loggerFactory->error($msg);
        break;
    }
  }

}
