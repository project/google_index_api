<?php

namespace Drupal\google_index_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\file\Entity\File;

/**
 * Class SettingsForm.
 */
class SettingsForm extends FormBase {

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\file\FileUsage\FileUsageInterface definition.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The State Service.
   * @param \Drupal\file\FileUsage\FileUsageInterface $file_usage
   *   The file usage service.
   */
  public function __construct(StateInterface $state, FileUsageInterface $file_usage) {
    $this->state = $state;
    $this->fileUsage = $file_usage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('file.usage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_index_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['info'] = [
      '#type' => 'item',
      '#title' => $this->t('Follow <a href="@url" target="_blank">these instructions</a> to setup and obtain your json file', [
        '@url' => "https://developers.google.com/search/apis/indexing-api/v3/prereqs"
      ]),
    ];

    $form['info_more'] = [
      '#type' => 'item',
      '#title' => $this->t('Also don\'t forget to <a href="@url" target="_blank">enable the Indexing API</a>', [
        '@url' => "https://console.developers.google.com/apis/enabled"
      ]),
    ];

    $form['top'] = [
      '#type' => 'fieldset',
      '#title' => t('Settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['top']['google_index_api_base_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The Base Domain of the site'),
      '#description' => 'We are setting this here since sometimes the base domain could be different on the backend.',
      '#default_value' => $this->state->get('google_index_api_base_domain'),
      '#required' => TRUE,
    ];

    $form['top']['google_index_api_json_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('The Client Service Account JSON File'),
      '#description' => 'This file contains your private key and other information',
      '#default_value' => $this->state->get('google_index_api_json_file'),
      '#upload_location' => 'private://google_index_api/',
      '#upload_validators'  => [
        'file_validate_extensions' => ['json'],
      ],
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Grab the values.
    $fid = $form_state->getValue('google_index_api_json_file');
    $domain = $form_state->getValue('google_index_api_base_domain');

    // Make the file perm.
    if (isset($fid[0])) {
      $file = File::load($fid[0]);
      if ($file !== NULL) {
        $file->status = FILE_STATUS_PERMANENT;
        $file->save();
        // Add to usage table.
        $uuid = $file->uuid();
        $this->fileUsage->add($file, 'google_index_api', 'google_index_api', $uuid);
      }
      else {
        $fid = NULL;
      }
    }
    else {
      $fid = NULL;
    }

    // Save to use else where.
    $this->state->set('google_index_api_json_file', $fid);
    $this->state->set('google_index_api_base_domain', $domain);
  }

}
