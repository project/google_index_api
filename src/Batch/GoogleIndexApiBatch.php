<?php

namespace Drupal\google_index_api\Batch;

/**
 * Class GoogleIndexApiBatch.
 *
 * @package Drupal\google_index_api
 */
class GoogleIndexApiBatch {

  /**
   * Common batch processing callback for all operations.
   *
   * @param string $url
   *   The url string we are checking.
   * @param object &$context
   *   The batch context object.
   */
  public static function batchProcess($url, &$context) {
    // Show message.
    $message = t('Now checking %url', ['%url' => $url]);
    $context['message'] = '<h2>' . $message . '</h2>';

    \Drupal::service('google_index_api.client')->updateUrl($url);

    // Set the result.
    $context['results']['final'][] = $url;
  }

  /**
   * Batch finished callback.
   */
  public static function batchFinished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(count($results), 'One URL updated.', '@count urls updated.');
      \Drupal::messenger()->addStatus($message);
    }
    else {
      $error_operation = reset($operations);
      \Drupal::messenger()->addError(t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
    }
  }

}
