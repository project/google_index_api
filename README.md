CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module provides a way to integrate with Google Indexing API to update or
delete urls in your domain property.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/google_index_api

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/google_index_api


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Google Indexing API module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420
   for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Web services >
       Google Index API > Settings for configuration.
    3. Navigate to Administration > Configuration > Web services >
       Google Index API > Bulk Update. This is handy if you need to update a
       slew of urls after a migration or some drastic change to the site.


MAINTAINERS
-----------

 * John Ouellet (labboy0276) - https://www.drupal.org/u/labboy0276

Supporting organizations:

 * Tandem - https://www.drupal.org/tandem
